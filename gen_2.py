#!/usr/bin/env python3
'''
sentences = [
    'she enjoys reading books in her free time while he prefers to read magazines',
    'they often go hiking in the mountains during summer as we enjoy camping by the lake',
    'the cat loves to nap in the sun all day while the dog eagerly awaits its evening walk',
    'the flower blooms brightly in the garden as the tree sways gently in the breeze',
    'the river flows peacefully through the valley while the wind blows softly through the trees',
    'the rain falls steadily on the roof as the snow melts slowly',
    'the mountain stands tall against the horizon and the building towers over the city skyline',
    'the car speeds down the highway while the train chugs along the tracks',
    'the moon casts a gentle glow on the landscape at night just as the sun sets behind the horizon every evening',
    'the clock ticks steadily on the wall while the alarm buzzes to wake up the heavy sleeper',
    'the baby cries softly in the nursery while the child laughs joyfully at the playground',
    'the artist paints a vibrant picture on the while the musician plays a soulful melody on the guitar',
    'the chef cooks a delicious meal in the restaurant kitchen and the serves the guests with a smile',
    'the teacher explains the lesson clearly to the class while the student studies diligently for the upcoming exam',
    'the doctor examines the patient symptoms carefully as nurse assists the surgeon during the operation',
    'the photographer captures fleeting moments precision while the athlete trains rigorously to improve performance',
    'the coach motivates the team to give their best effort and the manager supervises the staff to smooth operations',
    'the employee works diligently to meet the while the buyer purchases goods from the store',
    'the writer pens captivating stories that captivate readers while the reader reads voraciously devouring books'
]
'''
sentences = [
    "she dreams of traveling the world.",
    "traveling brings her joy and happiness.",
    "joy fills her heart with contentment.",
    "contentment is her constant state of being.",
    "being happy is her goal in life.",
    "life offers her new opportunities.",
    "opportunities lead to exciting adventures.",
    "adventures are her favorite part of life.",
    "part of her routine is daily exercise.",
    "exercise keeps her healthy and strong."
]
