#!/usr/bin/env python3
from tokenizing import Tokenizer, vocabulary

# 定义一个句子列表，后面会用这些句子来训练 CBOW 和 Skip-Gram 模型
origin_sentences = ["在这个快节奏的社会里，我们时常忙得 无暇顾及身心健康。无论是工作还是生活中，我们都需要保持良好的心态。面对困难和挑战，我们要学会坚定自信，勇敢前行。生活中总会有一些不如意的事情发生，但关键在于如何去面对。每个人都有 自己的梦想和目标，努力拼搏才能实现。心怀感恩，珍惜当下的幸福，将会让 生活更加美好。无论遇到什么困难，只要坚持下去，一切 终将 迎刃而解。感受生活的酸甜苦辣，才能更好地 体会其中的乐趣。人生 的 道路上充满了曲折和坎坷，但只有经历才能成长。每 个 人 都有 自己 的 闪光点，只要发掘并努力发挥，一定 能 取得 成功。我们齐心协力，共同创造更加美好的未来。时刻保持学习的心态，不断提升自我，才能立于败之地。.生活中 充满 了 无数 可能，只要肯去尝试，就能收获 不一样 的 惊喜。没有什么是一蹴而就，只有脚踏实地才能取得 成功。每一次风雨的洗礼，都是成长的机会和财富。保持一颗平常心，对待世间万物，能拥有真正的快乐。要学会面对失败和挫折从中总结经验，不断 完善 自己。真正的勇气，不是 毫 无 畏惧，而是在恐惧面前依然坚定。每一次付出和努力，都将成为未来成功的基石。人生 的 道路上 有 无数 的 叉口，选择对的方向才能走得更远。"]

sentences = []
tokenizer = Tokenizer(vocabulary)
for origin_sentence in origin_sentences:
    tokens = tokenizer.tokenize(origin_sentence)
    sentence = ""
    for _ in tokens:
        sentence += _[0]
        sentence += ' '
        pass
    sentence = sentence[:-1]
    sentences.append(sentence)
    pass
print(sentences)
for _ in sentences[0].split():
    print(_)
